part of 'pages.dart';

class CreatePasswordPage extends StatefulWidget {
  const CreatePasswordPage({Key? key}) : super(key: key);

  @override
  _CreatePasswordPageState createState() => _CreatePasswordPageState();
}

class _CreatePasswordPageState extends State<CreatePasswordPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
  }

  final FocusNode _passwordFocusNode = FocusNode();
  TextEditingController passwordController = TextEditingController();
  bool isPasswordValid = false,
      islowcase = false,
      is1Capital = false,
      is9Char = false,
      isnumber = false,
      _showPassword = false;
  String passStrength = '';

  RegExp rexUppercase = RegExp("(?=.*[A-Z])");
  RegExp rexNumber = RegExp("(?=.*[0-9])");
  RegExp rexLowercase = RegExp("(?=.*[a-z])");

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToWelcomePage());
        return false;
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: mainColor,
            // decoration: const BoxDecoration(
            // image: DecorationImage(
            //     image: AssetImage('assets/img/bg_welcome.png'),
            //     fit: BoxFit.fill),
            // ),
            // const EdgeInsets.only(top: 70, right: 5, left: 5, bottom: 20),
            child: Stack(
              children: [
                Positioned(
                  top: 40,
                  left: 20,
                  child: Row(
                    children: [
                      Container(
                        width: 18,
                        height: 15,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/img/back_arrow.png"),
                                fit: BoxFit.fill)),
                        child: GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToWelcomePage());
                          },
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Create Account',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 70, right: 5, left: 5, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/img/line_black.png'),
                                fit: BoxFit.fill)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '1',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '2',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '3',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '4',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 45,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Create Password',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            )),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text('Password will be used to login to account',
                            style: TextStyle(
                              height: 1.5,
                              color: Colors.white,
                              fontSize: 15,
                            )),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: TextFormField(
                          focusNode: _passwordFocusNode,
                          onChanged: (text) {
                            setState(() {
                              is9Char = text.length > 9;
                              is1Capital = rexUppercase.hasMatch(text);
                              islowcase = rexLowercase.hasMatch(text);
                              isnumber = rexNumber.hasMatch(text);
                              isPasswordValid = is9Char &&
                                  islowcase &&
                                  is1Capital &&
                                  isnumber;
                              if (text.isEmpty){
                                passStrength = '';
                              } else if (!isPasswordValid){
                                passStrength = 'Very Weak';
                              } else if (isPasswordValid){
                                passStrength = 'Very Strong';
                              }
                            });
                          },
                          controller: passwordController,
                          obscureText: !_showPassword,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1,
                                ),
                              ),
                              suffixIcon: GestureDetector(
                                onTap: () {
                                  _togglevisibility();
                                },
                                child: Icon(
                                  _showPassword
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.black,
                                ),
                              ),
                              hintText: 'Create Password'),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          children: [
                            const Text(
                              'Complexity:',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Text(
                              passStrength,
                              style:
                                  TextStyle(fontSize: 16, color: Colors.orange),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              children: [
                                islowcase
                                    ? Image.asset(
                                        'assets/img/check_green.png',
                                        height: 30,
                                        width: 30,
                                      )
                                    : const Text(
                                        'a',
                                        style: TextStyle(
                                            fontSize: 30, color: Colors.white),
                                      ),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text(
                                  'Lowercase',
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                is1Capital
                                    ? Image.asset(
                                        'assets/img/check_green.png',
                                        height: 30,
                                        width: 30,
                                      )
                                    : const Text(
                                        'A',
                                        style: TextStyle(
                                            fontSize: 30, color: Colors.white),
                                      ),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text(
                                  'Uppercase',
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                isnumber
                                    ? Image.asset(
                                        'assets/img/check_green.png',
                                        height: 30,
                                        width: 30,
                                      )
                                    : const Text(
                                        '123',
                                        style: TextStyle(
                                            fontSize: 30, color: Colors.white),
                                      ),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text(
                                  'Number',
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                is9Char
                                    ? Image.asset(
                                        'assets/img/check_green.png',
                                        height: 30,
                                        width: 30,
                                      )
                                    : const Text(
                                        '9+',
                                        style: TextStyle(
                                            fontSize: 30, color: Colors.white),
                                      ),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text(
                                  'Characters',
                                  style: TextStyle(
                                      fontSize: 13, color: Colors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 20,
                  right: 20,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        isPasswordValid
                            ? context.read<PageBloc>().add(GoToPersonalInformationPage())
                            : Fluttertoast.showToast(
                                msg:
                                    "Please fill lowercase, uppercase, number and 9 length password",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.SNACKBAR,
                                timeInSecForIosWeb: 1,
                                fontSize: 16.0);
                      });
                    },
                    child: const ButtonBlue(
                      label: 'Next',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _togglevisibility() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }
}
