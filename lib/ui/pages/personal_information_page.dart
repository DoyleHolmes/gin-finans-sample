part of 'pages.dart';

class PersonalInformationPage extends StatefulWidget {
  const PersonalInformationPage({Key? key}) : super(key: key);

  @override
  _PersonalInformationPageState createState() =>
      _PersonalInformationPageState();
}

class _PersonalInformationPageState extends State<PersonalInformationPage> {
  late Timer? timer = null;
  String valuedropdown = '- Choose Option -';
  String valuedropdown1 = '- Choose Option -';
  String valuedropdown2 = '- Choose Option -';

  @override
  void initState() {
    super.initState();

    Shared.save('firstdropdown', "2");
    Shared.save('valuedropdown', "- Choose Option -");
    Shared.save('valuedropdown1', "- Choose Option -");
    Shared.save('valuedropdown2', "- Choose Option -");
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    timer = Timer.periodic(const Duration(seconds: 1), (timer) async {
      valuedropdown = await Shared.read('valuedropdown') ?? "- Choose Option -";
      valuedropdown1 =
          await Shared.read('valuedropdown1') ?? "- Choose Option -";
      valuedropdown2 =
          await Shared.read('valuedropdown2') ?? "- Choose Option -";
      setState(() {
        valuedropdown;
        valuedropdown1;
        valuedropdown2;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToCreatePasswordPage());
        return false;
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: mainColor,
            // decoration: const BoxDecoration(
            // image: DecorationImage(
            //     image: AssetImage('assets/img/bg_welcome.png'),
            //     fit: BoxFit.fill),
            // ),
            // const EdgeInsets.only(top: 70, right: 5, left: 5, bottom: 20),
            child: Stack(
              children: [
                Positioned(
                  top: 40,
                  left: 20,
                  child: Row(
                    children: [
                      Container(
                        width: 18,
                        height: 15,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/img/back_arrow.png"),
                                fit: BoxFit.fill)),
                        child: GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToCreatePasswordPage());
                          },
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Create Account',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 70, right: 5, left: 5, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/img/line_black.png'),
                                fit: BoxFit.fill)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '1',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '2',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '3',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '4',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 45,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Personal Information',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            )),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                            'Please fill in the information below and your goal for digital saving',
                            style: TextStyle(
                              height: 1.5,
                              color: Colors.white,
                              fontSize: 15,
                            )),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: CustomDropdown(
                          label: 'Goal for activation',
                          defaultvalue: valuedropdown,
                          mode: 1,
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: CustomDropdown(
                          label: 'Monthly income',
                          defaultvalue: valuedropdown1,
                          mode: 2,
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: CustomDropdown(
                          label: 'Monthly expense',
                          defaultvalue: valuedropdown2,
                          mode: 3,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 20,
                  right: 20,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        context.read<PageBloc>().add(GoToScheduleVCPage());
                      });
                    },
                    child: const ButtonBlue(
                      label: 'Next',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
