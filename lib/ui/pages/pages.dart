// import 'package:djogja_trip/blocs/page_bloc.dart';
// import 'package:djogja_trip/shared/shared.dart';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:gin_finans/blocs/page/page_bloc.dart';
import 'package:gin_finans/helpers/helpers.dart';
import 'package:gin_finans/ui/widgets/widgets.dart';
import 'package:intl/intl.dart';

part 'wrapper.dart';
part 'welcome_page.dart';
part 'create_password_page.dart';
part 'personal_information_page.dart';
part 'schedule_video_call.dart';
