part of 'pages.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
  }

  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/img/bg_welcome.png'),
                  fit: BoxFit.fill),
            ),
            child: Container(
              margin:
                  const EdgeInsets.only(top: 70, right: 5, left: 5, bottom: 20),
              child: Stack(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/img/line_black.png'),
                                fit: BoxFit.fill)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '1',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '2',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '3',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '4',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 45,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Welcome to',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: RichText(
                          text: const TextSpan(
                            // style: const TextStyle(
                            //   fontSize: 14.0,
                            //   color: Colors.black,
                            // ),
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'GIN ',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 30,
                                  )),
                              TextSpan(
                                  text: 'Finans',
                                  // recognizer: TapGestureRecognizer()
                                  //   ..onTap = () => null,
                                  style: TextStyle(
                                    color: Color(0xFF4c92fc),
                                    fontSize: 30,
                                  )),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                            'Welcome to The Bank of The Future. Manage and track your accounts on the go.',
                            style: TextStyle(
                              height: 1.5,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            )),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: Colors.white,
                              ),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10))),
                          padding: const EdgeInsets.all(8),
                          child: TextFormField(
                            onTap: () {},
                            controller: emailController,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: const Color(0xFFDEDEDE),
                                prefixIcon: const Icon(Icons.email_outlined),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                    color: Color(0xFFDEDEDE),
                                    width: 1,
                                  ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                    color: Color(0xFFDEDEDE),
                                    width: 1,
                                  ),
                                ),
                                hintText: 'Email'),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                  Positioned(
                    bottom: 0,
                    left: 20,
                    right: 20,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          context.read<PageBloc>().add(GoToCreatePasswordPage());
                        });
                      },
                      child: const ButtonBlue(
                        label: 'Next',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
