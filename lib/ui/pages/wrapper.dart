part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PageBloc>(context).add(GoToWelcomePage());

    return BlocBuilder<PageBloc, PageState>(builder: (_, pageState) {
      if (pageState is OnWelcomePage) {
        return const WelcomePage();
      } else if (pageState is OnCreatePasswordPage) {
        return const CreatePasswordPage();
      } else if (pageState is OnPersonalInformationPage) {
        return const PersonalInformationPage();
      } else if (pageState is OnScheduleVCPage) {
        return const ScheduleVCPage();
      } else {
        return const WelcomePage();
      }
    });
  }
}
