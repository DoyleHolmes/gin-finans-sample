part of 'pages.dart';

class ScheduleVCPage extends StatefulWidget {
  const ScheduleVCPage({Key? key}) : super(key: key);

  @override
  _ScheduleVCPageState createState() => _ScheduleVCPageState();
}

class _ScheduleVCPageState extends State<ScheduleVCPage>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  late Timer? timer = null;
  String valuedropdown = '- Choose Option -';
  String valuedropdown1 = '- Choose Option -';
  String formattedDateTime = '';
  String formattedDateTime1 = '';

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    )
      ..forward()
      ..repeat(reverse: true);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<PageBloc>().add(GoToPersonalInformationPage());
        return false;
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: mainColor,
            // decoration: const BoxDecoration(
            // image: DecorationImage(
            //     image: AssetImage('assets/img/bg_welcome.png'),
            //     fit: BoxFit.fill),
            // ),
            // const EdgeInsets.only(top: 70, right: 5, left: 5, bottom: 20),
            child: Stack(
              children: [
                Positioned(
                  top: 40,
                  left: 20,
                  child: Row(
                    children: [
                      Container(
                        width: 18,
                        height: 15,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/img/back_arrow.png"),
                                fit: BoxFit.fill)),
                        child: GestureDetector(
                          onTap: () {
                            context.read<PageBloc>().add(GoToPersonalInformationPage());
                          },
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Create Account',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            )),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 70, right: 5, left: 5, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/img/line_black.png'),
                                fit: BoxFit.fill)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '1',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '2',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Color(0xff3b8c37),
                              number: '3',
                            ),
                            const SizedBox(
                              width: 45,
                            ),
                            const CircleWithNumber(
                              color: Colors.white,
                              number: '4',
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 100,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: Text('Schedule Video Call',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            )),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                            'Choose the date and time that you preferred, '
                            'we will send a link via email to make a video'
                            ' call on the scheduled date and time',
                            style: TextStyle(
                              height: 1.5,
                              color: Colors.white,
                              fontSize: 15,
                            )),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: GestureDetector(
                          onTap: (){
                            FocusScope.of(context).requestFocus(FocusNode());
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime.now(),
                                maxTime: DateTime(2050, 1, 1),
                                onConfirm: (date) {
                                  formattedDateTime = DateFormat('EEEE, dd MMM yyyy')
                                      .format(DateTime.parse('$date'))
                                      .toString();
                                  setState(() {
                                    valuedropdown = formattedDateTime;
                                  });
                                },
                                currentTime: DateTime.now(),
                                locale: LocaleType.id);
                          },
                          child: Container(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            width: MediaQuery.of(context).size.width,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.white,
                                border: Border.all(color: Colors.white, width: 1.5)),
                            child: Row(children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Date',
                                    style:  TextStyle(color: Colors.grey, fontSize: 12),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    valuedropdown,
                                    style: const TextStyle(color: Colors.black, fontSize: 17),
                                  ),
                                ],
                              ),
                              const Expanded(
                                flex: 1,
                                child: Text(" "),
                              ),
                              Container(
                                width: 20,
                                height: 20,
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                        image:  AssetImage('assets/img/arrow_down_grey.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ]),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: GestureDetector(
                          onTap: (){
                            FocusScope.of(context).requestFocus(FocusNode());
                            DatePicker.showTimePicker(context, showTitleActions: true,
                                onConfirm: (date) {
                                  formattedDateTime1 = DateFormat('HH:mm')
                                      .format(DateTime.parse('$date'))
                                      .toString();
                                  setState(() {
                                    valuedropdown1 = formattedDateTime1;
                                  });
                                },
                                currentTime: DateTime.now());
                          },
                          child: Container(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            width: MediaQuery.of(context).size.width,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.white,
                                border: Border.all(color: Colors.white, width: 1.5)),
                            child: Row(children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    'Time',
                                    style:  TextStyle(color: Colors.grey, fontSize: 12),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    valuedropdown1,
                                    style: const TextStyle(color: Colors.black, fontSize: 17),
                                  ),
                                ],
                              ),
                              const Expanded(
                                flex: 1,
                                child: Text(" "),
                              ),
                              Container(
                                width: 20,
                                height: 20,
                                decoration: const BoxDecoration(
                                    image: DecorationImage(
                                        image:  AssetImage('assets/img/arrow_down_grey.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 20,
                  top: 150,
                  child: AnimatedBuilder(
                    animation: animationController,
                    builder: (context, child) {
                      return Container(
                        decoration: ShapeDecoration(
                          color: Colors.white.withOpacity(0.5),
                          shape: const CircleBorder(),
                        ),
                        child: Padding(
                          padding:
                              EdgeInsets.all(8.0 * animationController.value),
                          child: child,
                        ),
                      );
                    },
                    child: Container(
                      decoration: const ShapeDecoration(
                        color: Colors.white,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        onPressed: () {},
                        color: Colors.blue,
                        icon: const Icon(Icons.calendar_today, size: 24),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 20,
                  right: 20,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        context.read<PageBloc>().add(GoToWelcomePage());
                      });
                    },
                    child: const ButtonBlue(
                      label: 'Next',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
