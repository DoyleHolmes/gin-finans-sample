part of 'widgets.dart';

class CustomDropdown extends StatefulWidget {
  final String label, defaultvalue;
  final int mode;

  const CustomDropdown(
      {Key? key,
      required this.label,
      required this.defaultvalue,
      required this.mode})
      : super(key: key);

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  late OverlayEntry _overlayEntry;
  int firstdropdown = 2;

  late Timer? timer = null;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (widget.mode != 4) {
          firstdropdown = int.parse(await Shared.read('firstdropdown') ?? '2');
          setState(() {
            int tempint1 = firstdropdown + 1;
            Shared.save('firstdropdown', tempint1.toString());
            firstdropdown = tempint1;
            try {
              if (firstdropdown % 2 == 0) {
                _overlayEntry.remove();
                if (timer != null) timer!.cancel();
              } else {
                _overlayEntry =
                    CustomOverlay().createOverlayEntry(context, widget.mode);
                Overlay.of(context)!.insert(_overlayEntry);
                if (timer != null) timer!.cancel();
                timer =
                    Timer.periodic(const Duration(seconds: 1), (timer) async {
                  int tempint2 =
                      int.parse(await Shared.read('firstdropdown') ?? '2');
                  setState(() {
                    firstdropdown = tempint2;
                  });
                  if (firstdropdown % 2 == 0) timer.cancel();
                });
              }
            } catch (e) {
              Shared.save('firstdropdown', firstdropdown.toString());
            }
          });
        }
      },
      child: Container(
        padding: const EdgeInsets.only(left: 8, right: 8),
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white,
            border: Border.all(color: Colors.white, width: 1.5)),
        child: Row(children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.label,
                // 'Goal for activation',
                style: const TextStyle(color: Colors.grey, fontSize: 12),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                widget.defaultvalue,
                style: const TextStyle(color: Colors.black, fontSize: 17),
              ),
            ],
          ),
          const Expanded(
            flex: 1,
            child: Text(" "),
          ),
          Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: firstdropdown % 2 == 0
                        ? const AssetImage('assets/img/arrow_down_grey.png')
                        : const AssetImage('assets/img/arrow_up_grey.png'),
                    fit: BoxFit.fill)),
          ),
        ]),
      ),
    );
  }
}
