part of 'widgets.dart';

class ButtonBlue extends StatefulWidget {
  final String? label, image;

  const ButtonBlue({Key? key, this.label, this.image}) : super(key: key);

  @override
  _ButtonBlueState createState() => _ButtonBlueState();
}

class _ButtonBlueState extends State<ButtonBlue> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(bottom: 10),
      decoration: const BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/img/btn_blue.png'), fit: BoxFit.fill),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          widget.image == null
              ? const Text('')
              : Container(
                  height: 20,
                  width: 25,
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/img/${widget.image}.png'),
                        fit: BoxFit.fill),
                  ),
                ),
          Text(
            widget.label.toString(),
            style: const TextStyle(color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
