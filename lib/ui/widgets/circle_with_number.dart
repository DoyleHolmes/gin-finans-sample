part of 'widgets.dart';

class CircleWithNumber extends StatefulWidget {
  final String number;
  final Color color;

  const CircleWithNumber({Key? key, required this.number, required this.color}) : super(key: key);

  @override
  _CircleWithNumberState createState() => _CircleWithNumberState();
}

class _CircleWithNumberState extends State<CircleWithNumber> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 50,
      child: Center(
        child: Text(
          widget.number,
          style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
      ),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 1),
          color: widget.color,
          shape: BoxShape.circle),
    );
  }
}
