part of 'widgets.dart';

class CustomOverlay {
  // final Function oonTap;
  // CustomOverlay({this.oonTap});

  OverlayEntry createOverlayEntry(BuildContext context, int mode) {
    RenderBox? renderBox = context.findRenderObject() as RenderBox?;
    var size = renderBox!.size;
    var offset = renderBox.localToGlobal(Offset.zero);
    // List<LanguageData> languageList = LanguageData.languageList();
    late OverlayEntry entry;

    return entry = OverlayEntry(
        builder: (context) => Positioned(
              left: offset.dx,
              top: offset.dy + size.height + 5.0,
              width: size.width,
              child: Material(
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                elevation: 4.0,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: ListView(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    children: <Widget>[
                      itemsample(context, entry, size, 'Sample 1', 0, 10, mode),
                      itemsample(context, entry, size, 'Sample 2', 0, 0, mode),
                      itemsample(context, entry, size, 'Sample 3', 10, 0, mode),
                    ],
                  ),
                ),
              ),
            ));
  }

  Widget itemsample(BuildContext context, OverlayEntry entry, Size size,
      String data, double edgeTop, double edgeBottom, int mode) {
    return GestureDetector(
      onTap: () async {
        if (mode == 1) {
          Shared.save('valuedropdown', data);
        } else if (mode == 2){
          Shared.save('valuedropdown1', data);
        } else if (mode == 3){
          Shared.save('valuedropdown2', data);
        }
        Shared.save('firstdropdown',
            (int.parse(await Shared.read('firstdropdown') ?? '2') + 1).toString());
        entry.remove();
      },
      child: Container(
        color: Colors.transparent,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: edgeTop, bottom: edgeBottom),
        child: Text(
          data,
          style: const TextStyle(fontSize: 15),
        ),
      ),
    );
  }
}
