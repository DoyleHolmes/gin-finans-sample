part of 'helpers.dart';

const double defaultMargin = 24;

Color mainColor = const Color(0xFF2c69c9);
Color accentColor1 = const Color(0xFF317ae8);
Color accentColor2 = const Color(0xFF4c92fc);
Color accentColor3 = const Color(0xFF31732d);