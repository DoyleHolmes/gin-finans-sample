part of 'page_bloc.dart';

abstract class PageState {
  const PageState();
}

class OnInitialPage extends PageState {

}

class OnWelcomePage extends PageState {

}

class OnCreatePasswordPage extends PageState {

}

class OnPersonalInformationPage extends PageState {

}

class OnScheduleVCPage extends PageState {

}