part of 'page_bloc.dart';

abstract class PageEvent {
  const PageEvent();
}

class GoToWelcomePage extends PageEvent {

}

class GoToCreatePasswordPage extends PageEvent {

}

class GoToPersonalInformationPage extends PageEvent {

}

class GoToScheduleVCPage extends PageEvent {

}