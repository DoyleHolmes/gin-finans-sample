import 'dart:async';

import 'package:bloc/bloc.dart';

part 'page_event.dart';

part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToWelcomePage) {
      yield OnWelcomePage();
    } else if (event is GoToCreatePasswordPage) {
      yield OnCreatePasswordPage();
    } else if (event is GoToPersonalInformationPage) {
      yield OnPersonalInformationPage();
    } else if (event is GoToScheduleVCPage) {
      yield OnScheduleVCPage();
    }
  }
}
