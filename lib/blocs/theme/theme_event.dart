part of 'theme_bloc.dart';

abstract class ThemeEvent {
  const ThemeEvent();
}

class ChangeTheme extends ThemeEvent {
  final ThemeData themeData;

  const ChangeTheme(this.themeData);
}